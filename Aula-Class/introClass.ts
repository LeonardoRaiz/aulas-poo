namespace introClass {
    class Casa {
        endereco: string;
        numero: number;
        CEP: string;
        tamTerreno: number;
        complemento: string;
        
        constructor() {
            this.endereco = "";
            this.numero = 0;
            this.CEP = "";
            this.tamTerreno = 0;
            this.complemento = "";
            this.cor = "";
        }
        cor: string;
        pintarCasa(novaCor: string): void {
            this.cor = novaCor;
            console.log(`A cor nova da casa é ${this.cor}`);
        } 
    }

    let casaSantucci = new Casa();
    let casaJuliana = new Casa();
    
    casaSantucci.endereco = "Rua dos teste da america";
    casaSantucci.numero = 412;
    casaSantucci.CEP = "14498-23";
    casaSantucci.cor = "Azul";
    casaSantucci.tamTerreno = 1500;

    casaJuliana.endereco = "Rua do basquete";
    casaJuliana.numero = 290;
    casaJuliana.CEP = "14987-23";
    casaJuliana.cor = "Vermelho";
    casaJuliana.complemento = "Bloco 1A";
    casaJuliana.tamTerreno = 5000;

    // console.log(casaSantucci);
    // console.log(casaJuliana);
    
    casaSantucci.pintarCasa("Preto");
    // console.log(casaSantucci);
    casaJuliana.pintarCasa("Rosa");
    // console.log(casaJuliana);
    
    // Crie uma classe de nome Animal com 2 metodos e 5 atributos.
    
    class Animal {
        nome: string;
        idade: number;
        raca: string;
        tutor: string;
        sexo: string;
        velocidade: number;
        cor: string;

        constructor(nome: string, idade: number, raca: string, cor: string, sexo: string) {
            this.nome = nome;
            this.idade = idade;
            this.raca = raca;
            this.cor = cor;
            this.sexo = sexo;
            this.tutor = "";
            this.velocidade = 0;
        }

        emitirSom(raca:string, som: string){
            console.log(`O ${raca} ${som}`);
        }

        hunt(nome:string){
            console.log(`O ${nome} está caçando!`);
        }
    }

    let gato = new Animal("Lupita", 17, "Indefinido", "Preto", "Feminino");
    gato.tutor = "Léo";
    gato.velocidade = 15;
    console.log(gato.nome);
    gato.emitirSom("gato", "miou");

    let cachorro = new Animal("Bobby", 8, "Basset", "Marrom", "Macho");
    cachorro.velocidade = 7;
    cachorro.hunt(cachorro.nome);
    
}