namespace exercicio_02
{
    let cor: string;
    console.log("Qual sua cor favorita?");

    cor = "Roxo";

    switch(cor)
    {
        case "Azul": console.log("Você escolheu azul");
                    break;
        case "Verde": console.log("Você escolheu verde.");
                    break;
        case "Vermelho": console.log("Você escolheu vermelho.");
                    break;
        case "Amarelo": console.log("Você escolheu amarelo.");
                    break;
        default: console.log(`Você escolheu ${cor}`);
    }
}