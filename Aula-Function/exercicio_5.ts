namespace exercicio_5 {
    let meuVetor:number[] = [6, 7, 2, 3, 11, 10];

    function maior1(vetor:number[]):number {
        return Math.max(...vetor);
    }
    function menor(vetor:number[]): number {
        return Math.min(...vetor)
    }

    function maior2(vetor:number[]):number {
        let maiorEle: number = vetor[0];
        vetor.forEach((num) => {
            if(num > maiorEle){
                maiorEle = num;
            }
        })
        return maiorEle;
    }

    function maior3(vetor:number[]):number {
        let maiorEle: number = vetor[0];
        for (let num of vetor) {
            if(num > maiorEle)
            {
                maiorEle = num;
            }
        }
        return maiorEle
    }
    
    console.log(maior1(meuVetor));
    console.log(maior2(meuVetor));
    console.log(maior3(meuVetor));
}
