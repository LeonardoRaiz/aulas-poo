//Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor. Em seguida, use o método map() para criar um novo array contendo apenas os títulos dos livros.
namespace exercicio_3 {
    /*let obj: any = {
        nome: "Leonardo",
        idade: 33,
        email: "leonardosenai604@gmail.com"
    }
    console.log(obj);*/
    let livros: any[] = [
        {titulo: "Titulo 1", autor:"Autor 1"},
        {titulo: "Titulo 2", autor:"Autor 2"},
        {titulo: "Titulo 3", autor:"Autor 3"},
        {titulo: "Titulo ab", autor:"Autor 3"},
        {titulo: "Titulo af", autor:"Autor 3"},
        {titulo: "Titulo 23", autor:"Autor 3"},
        {titulo: "Titulo 4", autor:"Autor 4"},
        {titulo: "Titulo 5", autor:"Autor 5"},
    ];
    let autores = livros.map((livro) => {
        return livro.autor
    });
    let titulos = livros.map((livro) => {
        return livro.titulo
    });
    console.log(autores);
    console.log(titulos);
    // Dado um array de objetos livros, contendo os campos titulo e autor, crie um programa em TypeScript que utilize a função filter() para encontrar todos os livros do autor com valor "Autor 3". Em seguida, utilize a função map() para mostrar apenas os títulos dos livros encontrados. O resultado deve ser exibido no console.

    let livrosAutor3 = livros.filter((livro) => {
        return livro.autor === "Autor 3" 
    })
    console.log(livrosAutor3);
    let titulosAutor3 = livrosAutor3.map((livro) => {
        return livro.titulo;
    })
    console.log(titulosAutor3);
}